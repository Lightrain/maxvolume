package com.lightrain.maxvolume;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;

public class MaxVolumeListen extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent arg1) {
		// TODO Auto-generated method stub
	
		AudioManager am = (AudioManager)context.getSystemService(context.AUDIO_SERVICE);
		
		am.setStreamVolume(AudioManager.STREAM_RING, am.getStreamMaxVolume(AudioManager.STREAM_RING), AudioManager.FLAG_SHOW_UI);
		
		am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
	}

}
